<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .main {
            padding: 5% 10%;
            margin: 0;
            box-sizing: border-box;
        }

        .main .search-form {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;


        }

        .search-form form {
            padding: 32px 48px;
            display: flex;
            flex-direction: column;
            width: 500px;
        }

        form .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 16px;
        }

        .form-group .form-label {
            align-items: center;
            display: flex;
            justify-content: center;
            width: 30%;
            text-align: center;
            margin-right: 16px;
            margin-bottom: 0;
            color: #000;

        }

        .form-group .form-input {
            width: 70%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: row;
            flex: 1 1 0;
        }

        .form-group .form-input input[type="text"] {
            width: 100%;
            font-size: 18px;
            background-color: #e1eaf4;
            padding: 8px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
        }

        .form-group .form-input #falcuties {
            width: 100%;
            font-size: 18px;
            background-color: #e1eaf4;
            padding: 10px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;

        }

        select {
            background: url("data:image/svg+xml,<svg height='20px' width='20px' viewBox='0 0 16 16' fill='%23000000' xmlns='http://www.w3.org/2000/svg'><path d='M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z'/></svg>") no-repeat;
            background-position: calc(100% - 0.75rem) center !important;
            -moz-appearance: none !important;
            -webkit-appearance: none !important;
            appearance: none !important;
            padding-right: 2rem !important;
        }

        form .btn {
            margin: auto;
        }

        .main .count {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 0 32px;
        }

        .count .student-found {
            /* margin: 8px 32px; */
        }

        .btn {
            text-align: center;
            width: 120px;
            color: #fff;
            background-color: #4f81bd;
            font-size: 14px;
            padding: 12px;
            border: 2px solid #385d8b;
            border-radius: 6px;
        }

        .count .addBtn {
            text-decoration: none;
            color: #fff;
        }

        .main .student-list {
            padding: 32px;
        }

        .student-list .student-table {
            width: 100%;
        }

        .student-list .student-table tr {
            width: 100%;
            height: 32px;
            /* margin-bottom: 16px; */
        }


        tr .action {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        tr .action .actionBtn {
            text-align: center;
            /* width: 80px; */
            color: #000;
            background-color: #e1eaf4;
            font-size: 12px;
            padding: 8px 12px;
            border: 1px solid #4f81bd;
            border-radius: 2px;
            cursor: pointer;

        }
    </style>
</head>

<body>
    <div class="main">
        <div class="search-form">
            <form action="" method="get">
                <div class="form-group">
                    <label class="form-label">Phân khoa</label>
                    <div class="form-input">
                        <select name="falcuty" id="falcuties">
                            <?php
                            $falcuty = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                            ?>
                        </select>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Từ khóa</label>
                    <div class="form-input">
                        <input type="text" name="keyword" id="keyword" value="">
                    </div>

                </div>
                <input type="submit" class="btn" value="Tìm kiếm"></input>
            </form>
        </div>
        <div class="count">
            <div class="student-found">
                <p>Số sinh viên tìm thấy: XXX</p>
            </div>
            <div class="btn">
                <a href="signup.php" class="addBtn">Thêm</a>
            </div>
        </div>
        <div class="student-list">
            <table class="student-table">
                <tr class="header">
                    <td style="width:20%">No.</td>
                    <td style="width:50%">Tên sinh viên</td>
                    <td style="width:30%">Khoa</td>
                    <td style="text-align:center;width:10%">Action</td>
                </tr>
                <tr class="student-item">
                    <td>1</td>
                    <td style="">Võ Xuân Hiếu</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>2</td>
                    <td style="">Võ Xuân Thành</td>
                    <td>Khoa học máy tính</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>3</td>
                    <td style="">Đỗ Viết Tuấn</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
                <tr class="student-item">
                    <td>4</td>
                    <td style="">Trần Xuân Vinh</td>
                    <td>Khoa học dữ liệu</td>
                    <td class="action">
                        <buton class="actionBtn" style="margin-right:8px;">Xóa</buton>
                        <buton class="actionBtn">Sửa</buton>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>